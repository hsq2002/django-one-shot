from django.shortcuts import render, redirect, get_object_or_404
from .models import TodoList
# Create your views here.


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_list": lists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    lists = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": lists,
    }
    return render(request, "todos/detail.html", context)


# def todo_list_create(request):
#     if request.method == "POST":
#         form = todo_create(request.POST)
#         if form.is_valid():
#             list_instance = form.save()
#         return redirect("detail_url", id=list_instance.id)

#     else:
#         form = todo_list_create()

#     context = {
#         "form": form
#     }

#     return render(request, "list_names/create.html", context)
